# Technologies

## Backend

<img src="nodejs.png" width="48px" height="48px" style="display: inline;" alt="Node.JS"/>
<img src="typeorm.png" width="128px" height="48px" style="display: inline;" alt="TypeORM"/>

For the backend I've chosen to use Node.JS with Typescript and TypeORM. The choice is a very suiting one, as it
provides most of the features the app would need, and being Node.JS, it is cross-platform and it helps the business
reach the market faster due to it's ecosystem. This also helps drag down costs, as many developers know a bit of
Javascript.

I've chosen a monolithic approach (again, due to time constraints), and I've chosen to use an onion architecture for
this project.

## Persistence

<img src="postgres.svg" width="48px" height="48px" style="display: inline;" alt="Postgres"/>
<img src="typeorm.png" width="128px" height="48px" style="display: inline;" alt="TypeORM"/>
<img src="rds.png" width="48px" height="48px" style="display: inline;" alt="AWS Relational Dataase Service"/>

I've chosen Postgres as my database of choice as it is a relational database, and with my kind of application, there
are many relations that need to be used. Postgres is free and open-source, it is highly performant, it is scalable and
it is supported by AWS RDS and TypeORM.

## Cache

<img src="redis.svg" width="48px" height="48px" style="display: inline;" alt="Redis"/>
<img src="elasticache.png" width="48px" height="48px" style="display: inline;" alt="Elastic Cache"/>

I've chosen Redis as my caching solution, as it is free, open-source, reliable and it is supported by AWS ElastiCache.

## Notifications

<img src="ses.svg" width="48px" height="48px" style="display: inline;" alt="AWS SES"/>
<img src="sns.svg" width="48px" height="48px" style="display: inline;" alt="AWS SNS"/>

For notifiying users I've chosen to use AWS SES and AWS SNS, reasons being costs, closeness to the infrastructure and
performance.

## Frontend

<img src="html5.svg" width="48px" height="48px" style="display: inline;" alt="HTML5"/>
<img src="css3.svg" width="48px" height="48px" style="display: inline;" alt="CSS3"/>
<img src="js.svg" width="48px" height="48px" style="display: inline;" alt="Javascript"/>

Vanilla javascript due to time-to-market constaints. I have to deliver this project really fast, so I won't be able to
use a library/framework for this. Vanilla javascript is sufficiently enough in order to build a functional web
application, with state management and dynamic rendering.

## Infrastructure

<img src="aws.png" width="48px" height="48px" style="display: inline;" alt="Amazon web services"/>
<img src="eb.png" width="48px" height="48px" style="display: inline;" alt="AWS Elastic Beanstalk"/>
<img src="rds.png" width="48px" height="48px" style="display: inline;" alt="AWS Relational Dataase Service"/>
<img src="elasticache.png" width="48px" height="48px" style="display: inline;" alt="Elastic Cache"/>

I've chosen to go with AWS on this project, due to it's low costs (this project can be done with less than $5 a month
using AWS), developer friendliness, reliability, scalability and tooling.

The application will be hosted using AWS ElasticBeanstalk, as it provides most of the tools needed for this project out
of the box (scalability, reliability, fault tolerance, speed, performance, low-ish costs).