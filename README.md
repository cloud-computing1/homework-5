# Calendary - a calendar application

## 1. Introduction

Calendars are essential time-keeping utilities that everyone uses daily in their lives, be it internally in their mind
or externally, using a paper calendar or electronic calendar.

In calendars we keep all the events we need throughout the day, or for the days to come, be it business events,
birthdays, activities or everything.

Calendary is an application that aims to help the user keep his/her life organised by providing a platform for him/her to use for managing events.

## 2. Case study

### The need for a digital calendar

Why use a digital calendar? A digital calendar doesn't lose track of time, notes. A digital calendar always reminds you
on time that you must attend an event. A digital calendar helps you schedule events from afar. A digital calendar can
be public if the owner wanted, and it can easily be shared with others. A digital calendar is an easy-to-use tool that
is always there for you, when you need it the must. It is a must-have tool.

### Why would our country use it?

Our country should use a digital calendar that can be shared with all its citizens so that every event is known by
everyone. Be it easter, christmas or president elections, we definitely need a handy assistant for this.
As of 2019, ~84% of the citizens have access to the internet, so it is about time for a digital revolution.

<img src="1. case study/internet.png" width="1024px" alt="Internet statistics for Europe, 2019"/>

### Why would other countries use it?

Most of the business people do already use it, but non-business users get by without one, or even worse, use a paper
one, which creates waste.

<img src="1. case study/phones.png" width="1024px" alt="Smartphone ownership statistics, 2021"/>

As of 2021, about 48.33% of people own a smartphone worldwide, and in the U.S.A., the rate is about 77%. This is
increasing year-by-year and more and more people get to use one. This means a huge possible market for digital
calendars, as the world is getting more and more digitalised.

## 3. Existing solutions

### Google calendar

<img src="2. existing solutions/google.png" width="128px" alt="Google calendar"/>

The most well-known solution for providing calendars is Google Calendar. The app was revolutionary when it was first
released, in 2009.

#### Architecture

The service provides a beautiful interface, and several clients (Web, Android, iOS) from which the user can choose.
It is backed by Google Cloud Platform, which is the platform Google uses to provide cloud services to developers and
businesses.

#### Marketing approaches

It was advertised by Google as their default calendars application. With such publicity, the app was sure to become
successful

#### Advantages/Disadvantages

Advantages|Disadvantages
-|-
Comes with every Google Account|Vendor lock-in
Is widely used|Has monopolistic tendencies
Highly scalable and available|
Uses lots of first-party control|
Free

<hr>

### Allcal

<img src="2. existing solutions/allcal.png" width="128px" alt="Allcal"/>

Another solution that is very close to this project. The app provides a simple interface and is free to use with
certain limits. It also provides additional features, such as a chat app, streams, tickets, permissions and so on.

#### Architecture

It is a classic client-server application, consisting of a Web, Android or iOS client, and a Web Server. From the IP
address, it seems to be hosted by AWS.

#### Marketing approaches

There were some ad campaigns in order to promote the service and it gained some traction in the U.S.A.

#### Advantages/Disadvantages

Advantages|Disadvantages
-|-
Works like a social network|Mostly targeted towards the U.S.A.
Has many services out-of-the-box|Not available globally
Targets event creators, artists|
Provides integrations with Google Calendar|

<hr>

### The native calendar on MacOS

<img src="2. existing solutions/macos-calendar.png" width="128px" alt="MacOS Calendar"/>

This is the native solution provided by Apple. It is able to connect with many services (such as Google Calendar).

#### Architecture

It consists of a native application written in Swift and out-of-the-box it works fully offline. If the user has an
iCloud connection available, it will automatically used, but the connection is not mandatory (it is mostly used for
syncing).

#### Marketing approaches

There was no need to do any marketing about it, as it is a native application and it is built-in the operating system

#### Advantages/Disadvantages

Advantages|Disadvantages
-|-
Very feature complete for a native out-of-the-box solution|Available only on MacOS
Is fast and offline|Cannot edit offline events without explicit access to the machine
Connects with many other calendar solutions|
Is native|

## 4. Business canvas

Here is the business canvas for this application.

<img src="3. business canvas/business-canvas.svg" width="2048px" alt="Business canvas"/>

## 5. Technologies

### Backend

<img src="5. technologies/nodejs.png" width="48px" height="48px" style="display: inline;" alt="Node.JS"/>
<img src="5. technologies/typeorm.png" width="128px" height="48px" style="display: inline;" alt="TypeORM"/>

For the backend I've chosen to use Node.JS with Typescript and TypeORM. The choice is a very suiting one, as it
provides most of the features the app would need, and being Node.JS, it is cross-platform and it helps the business
reach the market faster due to it's ecosystem. This also helps drag down costs, as many developers know a bit of
Javascript.

I've chosen a monolithic approach (again, due to time constraints), and I've chosen to use an onion architecture for
this project.

### Persistence

<img src="5. technologies/postgres.svg" width="48px" height="48px" style="display: inline;" alt="Postgres"/>
<img src="5. technologies/typeorm.png" width="128px" height="48px" style="display: inline;" alt="TypeORM"/>
<img src="5. technologies/rds.png" width="48px" height="48px" style="display: inline;" alt="AWS Relational Dataase Service"/>

I've chosen Postgres as my database of choice as it is a relational database, and with my kind of application, there
are many relations that need to be used. Postgres is free and open-source, it is highly performant, it is scalable and
it is supported by AWS RDS and TypeORM.

### Cache

<img src="5. technologies/redis.svg" width="48px" height="48px" style="display: inline;" alt="Redis"/>
<img src="5. technologies/elasticache.png" width="48px" height="48px" style="display: inline;" alt="Elastic Cache"/>

I've chosen Redis as my caching solution, as it is free, open-source, reliable and it is supported by AWS ElastiCache.

### Notifications

<img src="5. technologies/ses.svg" width="48px" height="48px" style="display: inline;" alt="AWS SES"/>
<img src="5. technologies/sns.svg" width="48px" height="48px" style="display: inline;" alt="AWS SNS"/>

For notifiying users I've chosen to use AWS SES and AWS SNS, reasons being costs, closeness to the infrastructure and
performance.

### Frontend

<img src="5. technologies/html5.svg" width="48px" height="48px" style="display: inline;" alt="HTML5"/>
<img src="5. technologies/css3.svg" width="48px" height="48px" style="display: inline;" alt="CSS3"/>
<img src="5. technologies/js.svg" width="48px" height="48px" style="display: inline;" alt="Javascript"/>

Vanilla javascript due to time-to-market constaints. I have to deliver this project really fast, so I won't be able to
use a library/framework for this. Vanilla javascript is sufficiently enough in order to build a functional web
application, with state management and dynamic rendering.

### Infrastructure

<img src="5. technologies/aws.png" width="48px" height="48px" style="display: inline;" alt="Amazon web services"/>
<img src="5. technologies/eb.png" width="48px" height="48px" style="display: inline;" alt="AWS Elastic Beanstalk"/>
<img src="5. technologies/rds.png" width="48px" height="48px" style="display: inline;" alt="AWS Relational Dataase Service"/>
<img src="5. technologies/elasticache.png" width="48px" height="48px" style="display: inline;" alt="Elastic Cache"/>

I've chosen to go with AWS on this project, due to it's low costs (this project can be done with less than $5 a month
using AWS), developer friendliness, reliability, scalability and tooling.

The application will be hosted using AWS ElasticBeanstalk, as it provides most of the tools needed for this project out
of the box (scalability, reliability, fault tolerance, speed, performance, low-ish costs).

## 6. C4 diagram

<img src="6. c4/Level1.png" width="2048px" style="display: inline;" alt="Level 1"/>
<img src="6. c4/Level2.png" width="2048px" style="display: inline;" alt="Level 2"/>
<img src="6. c4/Level3-Cache.png" width="2048px" style="display: inline;" alt="Level 3 - Cache"/>
<img src="6. c4/Level3-Database.png" width="2048px" style="display: inline;" alt="Level 3 - Database"/>
<img src="6. c4/Level3-WebApp.png" width="2048px" style="display: inline;" alt="Level 3 - Webapp"/>

## 7. Use-case diagram

<img src="7. use-case/use-case.svg" width="2048px" style="display: inline;" alt="Use-case diagram"/>
