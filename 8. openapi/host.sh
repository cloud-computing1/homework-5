#!/usr/bin/env bash

set -e

docker run -p 8080:8080 -e SWAGGER_JSON=/openapi/openapi.yaml -v "$PWD/8. openapi":/openapi swaggerapi/swagger-ui
