# Existing solutions

## Google calendar

<img src="google.png" width="128px" alt="Google calendar"/>

The most well-known solution for providing calendars is Google Calendar. The app was revolutionary when it was first
released, in 2009.

### Architecture

The service provides a beautiful interface, and several clients (Web, Android, iOS) from which the user can choose.
It is backed by Google Cloud Platform, which is the platform Google uses to provide cloud services to developers and
businesses.

### Marketing approaches

It was advertised by Google as their default calendars application. With such publicity, the app was sure to become
successful

### Advantages/Disadvantages

Advantages|Disadvantages
-|-
Comes with every Google Account|Vendor lock-in
Is widely used|Has monopolistic tendencies
Highly scalable and available|
Uses lots of first-party control|
Free

<hr>

## Allcal

<img src="allcal.png" width="128px" alt="Allcal"/>

Another solution that is very close to this project. The app provides a simple interface and is free to use with
certain limits. It also provides additional features, such as a chat app, streams, tickets, permissions and so on.

### Architecture

It is a classic client-server application, consisting of a Web, Android or iOS client, and a Web Server. From the IP
address, it seems to be hosted by AWS.

### Marketing approaches

There were some ad campaigns in order to promote the service and it gained some traction in the U.S.A.

### Advantages/Disadvantages

Advantages|Disadvantages
-|-
Works like a social network|Mostly targeted towards the U.S.A.
Has many services out-of-the-box|Not available globally
Targets event creators, artists|
Provides integrations with Google Calendar|

<hr>

## The native calendar on MacOS

<img src="macos-calendar.png" width="128px" alt="MacOS Calendar"/>

This is the native solution provided by Apple. It is able to connect with many services (such as Google Calendar).

### Architecture

It consists of a native application written in Swift and out-of-the-box it works fully offline. If the user has an
iCloud connection available, it will automatically used, but the connection is not mandatory (it is mostly used for
syncing).

### Marketing approaches

There was no need to do any marketing about it, as it is a native application and it is built-in the operating system

### Advantages/Disadvantages

Advantages|Disadvantages
-|-
Very feature complete for a native out-of-the-box solution|Available only on MacOS
Is fast and offline|Cannot edit offline events without explicit access to the machine
Connects with many other calendar solutions|
Is native|

