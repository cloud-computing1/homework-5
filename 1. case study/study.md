# Case study for Calendary - a calendar application

## Introduction

Calendars are essential time-keeping utilities that everyone uses daily in their lives, be it internally in their mind
or externally, using a paper calendar or electronic calendar.

In calendars we keep all the events we need throughout the day, or for the days to come, be it business events,
birthdays, activities or everything. We note all the events we know in it, and we must keep it organised, which is a
tedious task depending on how much time one has available.

## The need for a digital calendar

Why use a digital calendar? A digital calendar doesn't lose track of time, notes. A digital calendar always reminds you
on time that you must attend an event. A digital calendar helps you schedule events from afar. A digital calendar can
be public if the owner wanted, and it can easily be shared with others. A digital calendar is an easy-to-use tool that
is always there for you, when you need it the must. It is a must-have tool.

## Why would our country use it?

Our country should use a digital calendar that can be shared with all its citizens so that every event is known by
everyone. Be it easter, christmas or president elections, we definitely need a handy assistant for this.
As of 2019, ~84% of the citizens have access to the internet, so it is about time for a digital revolution.

<img src="internet.png" width="1024px" alt="Internet statistics for Europe, 2019"/>

## Why would other countries use it?

Most of the business people do already use it, but non-business users get by without one, or even worse, use a paper
one, which creates waste.

<img src="phones.png" width="1024px" alt="Smartphone ownership statistics, 2021"/>

As of 2021, about 48.33% of people own a smartphone worldwide, and in the U.S.A., the rate is about 77%. This is
increasing year-by-year and more and more people get to use one. This means a huge possible market for digital
calendars, as the world is getting more and more digitalised.