workspace {

    model {
        user = person "User" "A user of Calendary"
        calendary = softwareSystem "Software System" "The Calendary application" {
            webapp = container "Web application + backend" "The application" "Node.JS" {
                static = group "Static" {
                    templateFiles = component "Template files" "HTML templates for the frontend" "Pug"
                    staticFiles = component "Static files" "Static files used for the frontend (js + css + images)" "Express static (Javascript, CSS3, PNG)"
                }
                database = group "Database" {
                    databaseClient = component "Database instance" "Connection to the database, used for persisting users, calendars, followers, events, RSVPs" "TypeORM + Postgres"
                }
                cache = group "Cache" {
                    cacheClient = component "Cache instance" "Connection to the cache, used for sessions and CSRF tokens" "TypeORM + Postgres"
                }

                errors = group "Errors" {
                    errorsController = component "Errors controller" "Controlls behavior for when errors happen" "Typescript"
                    errorsService = component "Errors service" "Business logic for when errors happen, such as handling known errors" "Typescript"
                }
                crypto = group "Crypto" {
                    cryptoService = component "Crypto service" "Creates random strings of specified length, hashes text" "Typescript"
                }
                csrf = group "CSRF" {
                    csrfService = component "CSRF service" "Creates tokens for the frontend to use" "Typescript"
                    csrfRepository = component "CSRF repository" "Stores tokens" "Typescript"
                }
                validations = group "Validations" {
                    validationsService = component "Validations service" "Performs generic validations, such as email, password, text, number or date validations" "Typescript"
                }
                sessions = group "Sessions" {
                    sessionsRoutes = component "Sessions routes" "Handles routing for requests related to sessions + authentication (if needed): login, logout" "Express"
                    sessionsController = component "Sessions controller" "Does validations for session requests, handles session validations, controlls behavior for chosen route" "Typescript"
                    sessionsService = component "Sessions service" "Maps sessions, creates sessions, removes sessions, checks for session validity, gets user for session" "Typescript"
                    sessionsRepository = component "Sessions repository" "Creates sessions, gets sessions, removes sessions" "Typescript"
                }
                users = group "Users" {
                    usersRoutes = component "Users routes" "Handles routing for requests related to users + authentication (if needed): create user, get users, edit user, delete user" "Express"
                    usersController = component "Users controller" "Does validations for user requests, controlls behavior for chosen route" "Typescript"
                    usersService = component "Users service" "Maps users, creates users and creates a calendar for them, gets users by their ID, gets users by their email and password, gets user by email, edits user, deletes user" "Typescript"
                    usersRepository = component "Users repository" "Creates users, gets users by either userId, email and password, or email, edits user, deletes user" "Typescript"
                }
                calendars = group "Calendars" {
                    calendarsRoutes = component "Calendars routes" "Handles routing for requests related to calendars + authentication: create calendar, get public and private calendars, get calendar by id, edit calendar, delete calendar" "Express"
                    calendarsController = component "Calendars controller" "Does validations for calendar requests, controlls behavior for chosen route" "Typescript"
                    calendarsService = component "Calendars service" "Maps calendars, creates calendars, gets public or private calendars, gets calendar by id, edits calendar, deletes calendar" "Typescript"
                    calendarsRepository = component "Calendars repository" "Create calendars, retrieves calendars, edits calendars, deletes calendars" "Typescript"
                }
                followers = group "Followers" {
                    followersRoutes = component "Followers routes" "Handles routing for requests related to followers + authentication: follow calendar, unfollow calendar" "Express"
                    followersController = component "Followers controller" "Does validations for follower requests, controlls behavior for chosen route" "Typescript"
                    followersService = component "Followers service" "Maps followers, creates follower, gets followers for calendar, deletes followers for calendar" "Typescript"
                    followersRepository = component "Followers repository" "Creates follower, gets followers either by followerId, or userId and calendarId, deletes followers" "Typescript"
                }
                events = group "Events" {
                    eventsRoutes = component "Events routes" "Handles routing for requests related to events + authentication: create events, get events, edit events, delete events" "Express"
                    eventsController = component "Events controller" "Does validations for events requests, controlls behavior for chosen route" "Typescript"
                    eventsService = component "Events service" "Maps events, creates events, gets events by either eventId, userId, or userId and calendarId" "Typescript"
                    eventsRepository = component "Events repository" "Creates events, gets events by either eventId, userId of the owner, calendarId of the owner calendar, userId, or by userId and calendarId" "Typescript"
                }
                rsvps = group "RSVPs" {
                    rsvpsRoutes = component "RSVPs routes" "Handles routing for requests related to RSVPs + authentication: create RSVP for event, get RSVP for event, edit RSVP for event" "Express"
                    rsvpsController = component "RSVPs controller" "Does validations for events requests, controlls behavior for chosen route" "Typescript"
                    rsvpsService = component "RSVPs service" "Maps RSVPs, creates RSVPs, gets RSVPs by either rsvpId, userId, or by userId and eventId, edits RSVP, deletes RSVP" "Typescript"
                    rsvpsRepository = component "RSVPs repository" "Creates RSVPs, gets RSVPs by either rsvpId, userId, or by userId and eventId, edits RSVP status, deletes RSVP" "Typescript"
                }
                files = group "Files" {
                    filesRoutes = component "Files routes" "Handles routing for requests related to HTML templates" "Express"
                    filesController = component "Files controller" "Creates CSRF tokens and serves HTML templates" "Typescript"
                }
            }
            database = container "Database" "Used for storing user data, calendars, followers, events and RSVPs" "Postgres" "Database" {
                postgres = component "Database" "The database solution" "Postgres"
            }
            cache = container "Cache" "Used for storing sessions + CSRF tokens" "Redis" "Cache" {
                redis = component "Cache" "The caching solution" "Redis"
            }
        }
        ses = softwareSystem "AWS SES" "An email sending service"
        sns = softwareSystem "AWS SNS" "A notifications sending service"
        
        environment = deploymentEnvironment "Environment" {
            webappInstances = deploymentGroup "WebApp instances"
            databaseClients = deploymentGroup "Database instances"
            cacheClients = deploymentGroup "Cache instances"

            deploymentNode "WebApp" "A webapp deployment node" "AWS EB" {
                containerInstance webapp webappInstances {
                    healthCheck "check" "https://calendary.some.subdomain.from.amazonaws.com/management/healthz" 10
                }
            }
            
            deploymentNode "Database" "A database deployment node" "AWS EC2" {
                containerInstance database databaseClients
            }
            
            deploymentNode "Cache" "A cache deployment node" "AWS EC2" {
                containerInstance cache cacheClients
            }
        }

        # systtem context
        user -> webapp "Uses" "HTTPs"

        # container context
        webapp -> database "Reads from and writes to" "TypeORM + Postgres"
        webapp -> cache "Reads from and writes to" "Redis"
        webapp -> ses "Sends events to send email" "HTTPs"
        webapp -> sns "Sends events to send notifications" "HTTPs"
        ses -> user "Sends emails" "SMTP"
        sns -> user "Sends notifications" "TCP/IP"
        
        # component context
        user -> sessionsRoutes "Calls" "HTTPs"
        user -> usersRoutes "Calls" "HTTPs"
        user -> calendarsRoutes "Calls" "HTTPs"
        user -> followersRoutes "Calls" "HTTPs"
        user -> eventsRoutes "Calls" "HTTPs"
        user -> rsvpsRoutes "Calls" "HTTPs"
        user -> filesRoutes "Calls" "HTTPs"
        
        errorsController -> errorsService "Uses for handling business logic"
        
        csrfService -> csrfRepository "Uses for entity creation"

        sessionsRoutes -> sessionsController "Uses controller for requests"
        sessionsController -> validationsService "Uses for handling simple validations"
        sessionsController -> csrfService "Uses for handling validations for frontend"
        sessionsController -> usersService "Uses for handling validations for username and password"
        sessionsController -> sessionsService "Uses for handling business logic for requests"
        sessionsService -> cryptoService "Used for creating sessions"
        sessionsService -> sessionsRepository "Uses for handling entity creation"
        
        usersRoutes -> sessionsController "Uses for handling authentication + CSRF token validation"
        usersRoutes -> usersController "Uses controller for requests"
        usersController -> validationsService "Uses for handling simple validations"
        usersController -> usersService "Uses for handling business logic for requests"
        usersService -> cryptoService "Uses for handling password hashing"
        usersService -> calendarsService "Uses for handling default calendar creation"
        usersService -> usersRepository "Uses for handling entity creation"
    
        calendarsRoutes -> sessionsController "Uses for handling authentication + CSRF token validation"
        calendarsRoutes -> eventsController "Uses for handling retrieval of events for calendar"
        calendarsRoutes -> followersController "Uses for handling retrieval of followers for calendar"
        calendarsRoutes -> rsvpsController "Uses for handling retrieval of RSVPs for an event in a calendar"
        calendarsRoutes -> calendarsController "Uses controller for requests"
        calendarsController -> validationsService "Uses for handling simple validations"
        calendarsController -> calendarsService "Uses for handling business logic for requests"
        calendarsService -> followersRepository "Uses for handling default follower creation"
        calendarsService -> calendarsRepository "Uses for handling entity creation"
    
        followersRoutes -> sessionsController "Uses for handling authentication + CSRF token validation"
        followersRoutes -> followersController "Uses controller for requests"
        followersController -> validationsService "Uses for handling simple validations"
        followersController -> calendarsService "Uses for handling validation that the calendar exists before follower creation"
        followersController -> followersService "Uses for handling business logic for requests"
        followersService -> followersRepository "Uses for handling entity creation"
    
        eventsRoutes -> sessionsController "Uses for handling authentication + CSRF token validation"
        eventsRoutes -> rsvpsController "Uses for handling retrieval of RSVPs for event"
        eventsRoutes -> eventsController "Uses controller for requests"
        eventsController -> validationsService "Uses for handling simple validations"
        eventsController -> calendarsService "Uses for handling validation that the calendar exists before event creation"
        eventsController -> eventsService "Uses for handling business logic for requests"
        eventsService -> rsvpsRepository "Uses for handling default rsvp creation"
        eventsService -> eventsRepository "Uses for handling entity creation"
        
        rsvpsRoutes -> sessionsController "Uses for handling authentication + CSRF token validation"
        rsvpsRoutes -> rsvpsController "Uses controller for requests"
        rsvpsController -> validationsService "Uses for handling simple validations"
        rsvpsController -> eventsService "Uses for handling validation that the event exists before RSVP creation"
        rsvpsController -> rsvpsService "Uses for handling business logic for requests"
        rsvpsService -> rsvpsRepository "Uses for handling entity creation"

        filesRoutes -> filesController "Uses controller for requests"
        filesController -> templateFiles "Uses as reference"
        filesController -> staticFiles "Uses as reference"

        sessionsRepository -> cacheClient "Uses as storage"
        csrfRepository -> cacheClient "Uses as storage"
        usersRepository -> databaseClient "Uses as storage"
        calendarsRepository -> databaseClient "Uses as storage"
        followersRepository -> databaseClient "Uses as storage"
        eventsRepository -> databaseClient "Uses as storage"
        rsvpsRepository -> databaseClient "Uses as storage"

        cryptoService -> errorsController "Uses for handling errors if they are thrown"
        csrfService -> errorsController "Uses for handling errors if they are thrown"
        sessionsService -> errorsController "Uses for handling errors if they are thrown"
        usersService -> errorsController "Uses for handling errors if they are thrown"
        calendarsService -> errorsController "Uses for handling errors if they are thrown"
        followersService -> errorsController "Uses for handling errors if they are thrown"
        eventsService -> errorsController "Uses for handling errors if they are thrown"
        rsvpsService -> errorsController "Uses for handling errors if they are thrown"
        filesController -> errorsController "Uses for handling errors if they are thrown"
        
        databaseClient -> database "Uses for storage" "Postgres"
        cacheClient -> cache "Uses for storage" "Redis"
        usersService -> ses "Notifies"
        followersService -> ses "Notifies"
        followersService -> sns "Notifies"
        eventsService -> ses "Notifies"
        eventsService -> sns "Notifies"
        rsvpsService -> ses "Notifies"
        rsvpsService -> sns "Notifies"
    }

    views {
        systemContext calendary "SoftwareSystem" "The Calendary application" {
            include *
            autoLayout
        }
        
        container calendary "Application" "The application" {
            include *
            autoLayout
        }
        
        component webapp "WebApp" "The application" {
            include *
            autoLayout
        }
        
        component database "Database" "The database" {
            include *
            autoLayout
        }
        
        component cache "Cache" "The cache" {
            include *
            autoLayout
        }
        
        deployment calendary environment "Deployment" "Deployment process" {
            include *
            autoLayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            element "Database" {
                background #85bbf0
                shape Cylinder
            }
            element "Cache" {
                background #f08586
                shape Cylinder
            }
        }
    }
    
}